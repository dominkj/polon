package polon;

import org.json.JSONObject;

public class ProjectSpec {
    public String keywords;
    public String projectNumber;
    public String projectTitle;
    public String projectManager;
    public String contractorsNumber;
    public String projectStatus;
    public String agreementDate;
    public String projectEndDate;
    public String projectKind;
    public String programName;


    public JSONObject toJSON() {
         JSONObject jsonObject = new JSONObject()
                .put("keywords",keywords)
                .put("projectNumber",projectNumber)
                .put("projectTitle", projectTitle)
                .put("projectManager",projectManager)
                .put("contractorNumber",contractorsNumber)
                .put("projectStatus",projectStatus)
                .put("agreementDate",agreementDate)
                .put("projectEndDate",projectEndDate)
                .put("projectKind",projectKind)
                .put("programName",programName);



        return jsonObject;
    }

    public void printJSON(){
        System.out.println(toJSON().toString());
    }
}
