package polon;

import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class HttpRequests {

    Executor executor;
    CookieStore cookieStore;

    {
        cookieStore = new BasicCookieStore();
        HttpClient httpClient = HttpClientBuilder.create()
                //.setProxy(HttpHost.create("localhost:8080"))
                .setDefaultCookieStore(cookieStore)
                .setUserAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0")
                .build();

        executor = Executor.newInstance(httpClient);

    }

    public void entryPoint() throws IOException {
        executor.execute(
                Request.Get("https://polon.nauka.gov.pl/opi/aa/pn")
        ).discardContent();
    }

    public String nextPage() throws IOException {
        List<NameValuePair> formParams = new ArrayList<>();
//        formParams.add(new BasicNameValuePair("filter", "filter"));
//        formParams.add(new BasicNameValuePair("filter:j_idt158-value", "true"));
//        formParams.add(new BasicNameValuePair("filter:tableScienceProjects:j_jdt311:headerDataScroller1:page", "next"));
//        formParams.add(new BasicNameValuePair("filter:extraFiltersPanel-value", "true"));
//        formParams.add(new BasicNameValuePair("javax.faces.ViewState", "e1s1"));
//        formParams.add(new BasicNameValuePair("javax.faces.partial.ajax", "true"));
//        formParams.add(new BasicNameValuePair("javax.faces.source", "filter:tableScienceProjects:j_jdt311:headerDataScroller1"));
//        formParams.add(new BasicNameValuePair("AJAX:EVENTS_COUNT", "1"));
//        formParams.add(new BasicNameValuePair("filter:tableScienceProjects:j_jdt311:headerPageLength1:lengthSelectOneListbox1", "100"));
//        formParams.add(new BasicNameValuePair("filter:tableScienceProjects:j_jdt311:headerExport1:exportType", "ALL"));
//        formParams.add(new BasicNameValuePair("filter:tableScienceProjects:j_jdt586:headerPageLength1:lengthSelectOneListbox1", "100"));
//        formParams.add(new BasicNameValuePair("filter:tableScienceProjects:j_jdt586:headerExport1:exportType", "ALL"));

        formParams.add(new BasicNameValuePair("filter", "filter"));
        formParams.add(new BasicNameValuePair("filter:j_idt158-value", "true"));
        formParams.add(new BasicNameValuePair("filter:fullTextSearch", ""));
        formParams.add(new BasicNameValuePair("filter:institutionFilter:filterValue", ""));
        formParams.add(new BasicNameValuePair("filter:institutionFilter:filterValueUid", ""));
        formParams.add(new BasicNameValuePair("filter:projectTitleAutoInput:inputText", ""));
        formParams.add(new BasicNameValuePair("filter:extraFiltersPanel-value", "true"));
        formParams.add(new BasicNameValuePair("filter:projectKindCombo", ""));
        formParams.add(new BasicNameValuePair("filter:programNameAutoValue", ""));
        formParams.add(new BasicNameValuePair("filter:programNameAutoInput", ""));
        formParams.add(new BasicNameValuePair("filter:instFundNameAutoValue", ""));
        formParams.add(new BasicNameValuePair("filter:instFundNameAutoInput", ""));
        formParams.add(new BasicNameValuePair("filter:projectStatusCombo", ""));
        formParams.add(new BasicNameValuePair("filter:keywordsAutoInput:inputText", ""));
        formParams.add(new BasicNameValuePair("filter:classificationGboardCombo", ""));
        formParams.add(new BasicNameValuePair("filter:managerFirstName", ""));
        formParams.add(new BasicNameValuePair("filter:managerLastName", ""));
        formParams.add(new BasicNameValuePair("filter:areaFilter", ""));
        formParams.add(new BasicNameValuePair("filter:tableScienceProjects:j_idt311:headerPageLength1:lenghtSelectOneListbox1", "100"));
        formParams.add(new BasicNameValuePair("filter:tableScienceProjects:j_idt311:headerExport1:exportType", "ALL"));
        formParams.add(new BasicNameValuePair("filter:tableScienceProjects:j_idt586:headerPageLength1:lenghtSelectOneListbox1", "100"));
        formParams.add(new BasicNameValuePair("filter:tableScienceProjects:j_idt586:headerExport1:exportType", "ALL"));
        formParams.add(new BasicNameValuePair("javax.faces.ViewState", "e1s1"));/////
        formParams.add(new BasicNameValuePair("javax.faces.source", "filter:tableScienceProjects:j_idt311:headerDataScroller1"));
        formParams.add(new BasicNameValuePair("javax.faces.partial.event", "rich:datascroller:onscroll"));
        formParams.add(new BasicNameValuePair("javax.faces.partial.execute", "filter:tableScienceProjects:j_idt311:headerDataScroller1 @component"));
        formParams.add(new BasicNameValuePair("javax.faces.partial.render", "@component"));
        formParams.add(new BasicNameValuePair("filter:tableScienceProjects:j_idt311:headerDataScroller1:page", "next"));
        formParams.add(new BasicNameValuePair("org.richfaces.ajax.component", "filter:tableScienceProjects:j_idt311:headerDataScroller1"));
        formParams.add(new BasicNameValuePair("filter:tableScienceProjects:j_idt311:headerDataScroller1", "filter:tableScienceProjects:j_idt311:headerDataScroller1"));
        formParams.add(new BasicNameValuePair("rfExt", "null"));
        formParams.add(new BasicNameValuePair("AJAX:EVENTS_COUNT", "1"));
        formParams.add(new BasicNameValuePair("javax.faces.partial.ajax", "true"));


        String addrParm = ";jsessionid=" + cookieStore.getCookies().get(0).getValue();

        return executor.execute(
                Request.Post("https://polon.nauka.gov.pl/opi/aa/pn" + addrParm + "?execution=e1s1")
                        .addHeader("Faces-Request", "partial/ajax")
                        .addHeader("Origin", "https://polon.nauka.gov.pl")
                        .body(
                                EntityBuilder.create().
                                        setParameters(formParams)
                                        .setContentType(ContentType.APPLICATION_FORM_URLENCODED.withCharset("UTF-8"))

                                        .build())
        ).returnContent().asString();


    }

    public ProjectSpec getById(String id) throws IOException {

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String resp = executor.execute(
                Request.Get("https://polon.nauka.gov.pl/opi/aa/pn/szczegoly?returnURL=aa%2Fpn&projectId=" + id) ///17798
        ).returnContent().asString();

        Document doc = Jsoup.parse(resp);
        ProjectSpec projectSpec = new ProjectSpec();

        projectSpec.keywords = doc.select("[id='pnMainForm:keywords']").text();
        projectSpec.projectNumber = doc.select("[id='pnMainForm:projectNumber']").text();
        projectSpec.projectTitle = doc.select("[id='pnMainForm:projectTitle']").text();
        projectSpec.projectManager = doc.select("[id='pnMainForm:projectManager']").text();
        projectSpec.contractorsNumber = doc.select("[id='pnMainForm:contractorsNumber']").text();
        projectSpec.projectStatus = doc.select("[id='pnMainForm:projectStatus']").text();
        projectSpec.agreementDate = doc.select("[id='pnMainForm:agreementDate']").text();
        projectSpec.projectEndDate = doc.select("[id='pnMainForm:projectEndDate']").text();
        projectSpec.projectKind = doc.select("[id='pnMainForm:projectKind']").text();
        projectSpec.programName = doc.select("[id='pnMainForm:programName']").text();


        String executionNum = doc.select("[id='javax.faces.ViewState']").get(0).attr("value");

        List<NameValuePair> formParams = new ArrayList<>();
        formParams.add(new BasicNameValuePair("pnMainForm", "pnMainForm"));
        formParams.add(new BasicNameValuePair("pnMainForm:tabPanel-value", "jednostkiRealizujace"));
        formParams.add(new BasicNameValuePair("pnMainForm:j_idt4201-value", "danePodstawowePodgladEdycja"));
        formParams.add(new BasicNameValuePair("pnMainForm:j_idt4339-value", "false"));
        formParams.add(new BasicNameValuePair("javax.faces.ViewState", executionNum));
        formParams.add(new BasicNameValuePair("javax.faces.source", "pnMainForm:j_idt4730"));
        formParams.add(new BasicNameValuePair("javax.faces.partial.execute", "pnMainForm:j_idt4730@component"));
        formParams.add(new BasicNameValuePair("javax.faces.partial.render", "@component"));
        formParams.add(new BasicNameValuePair("org.richfaces.ajax.component", "pnMainForm:j_idt4730"));
        formParams.add(new BasicNameValuePair("pnMainForm:j_idt4730", "pnMainForm:j_idt4730"));
        formParams.add(new BasicNameValuePair("rfExt", "null"));
        formParams.add(new BasicNameValuePair("AJAX:EVENTS_COUNT", "1"));
        formParams.add(new BasicNameValuePair("javax.faces.partial.ajax", "true"));
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


//        resp = executor.execute(
//                Request.Post("https://polon.nauka.gov.pl/opi/aa/pn/szczegoly?execution=" + executionNum)
//                        .addHeader("Faces-Request", "partial/ajax")
//                        .addHeader("Origin", "https://polon.nauka.gov.pl")
//                        .addHeader("Referer", "https://polon.nauka.gov.pl/opi/aa/pn/szczegoly?execution=e2s1")
//                        .addHeader("DNT","1")
//                        .body(
//                                EntityBuilder.create().
//                                        setParameters(formParams)
//                                        .setContentType(ContentType.APPLICATION_FORM_URLENCODED.withCharset("UTF-8"))
//
//                                        .build())
//        ).returnContent().asString();

        doc = Jsoup.parse(resp);

//        resp = executor.execute(
//                Request.Get("https://polon.nauka.gov.pl" + doc.select("redirect").attr("url"))
//        ).returnContent().asString();

        doc = Jsoup.parse(resp);


        return projectSpec;

    }


}
